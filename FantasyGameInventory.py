# -*- coding: utf-8 -*-

# created by Anna Sekuła on 12.03.18


def displayInventory(_inventory):
    """
    :param dict[str, int] _inventory: Items and their count.
    """
    print('Inventory:')
    for item, number in _inventory.items():
        print(number, item)
    print('Total number of items: {}'.format(sum(_inventory.values())))


def addToInventory(_inventory, added_items):
    """
    :param dict[str, int] _inventory: Items and their count.
    :param list[str] added_items: New items.
    :returns: Extended inventory.
    :rtype: dict[str, int]
    """
    for item in added_items:
        if item in _inventory.keys():
            _inventory[item] += 1
        else:
            _inventory[item] = 1
    return _inventory


inventory = {'rope': 1, 'torch': 6, 'gold coin': 42,
             'dagger': 1, 'arrow': 12}
items = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
displayInventory(addToInventory(inventory, items))
