#!/usr/bin/env python3


"""
Simple time tracker.
"""


import os, sqlite3, sys, time, datetime


def createDatabase():
    """
    Creating a database for tracking entries.
    Database is created in ~/.config/TimeTracker/.
    """
    db_directory = os.path.expanduser('~/.config/TimeTracker/')
    if not os.path.exists(db_directory):
        os.makedirs(db_directory)
    return sqlite3.connect(os.path.join(db_directory, 'tracking.db'))


def createTable(query):
    """
    Creating a database table.
    :param str query: SQL query.
    """
    cursor.execute(query)


def menu():
    """
    Printing out app menu and running respective methods on choice.
    """
    _menu = {'A': 'add project', 'P': 'show projects',
             'T': 'track time', 'S': 'stop tracking',
             'D': 'display total time', 'E': 'exit'}
    while True:
        print('\n\n=======================')
        print('===== TimeTracker =====')
        print('=======================\n')
        for option in _menu.keys():
            print('{}: {}'.format(option, _menu[option]))
        print('[Ctrl-C back to main menu]')
        selected = input('\nSelect an option: ').lower().strip()
        if selected == 'a':
            try:
                name = input('\nEnter project name: ')
            except KeyboardInterrupt:
                menu()
            addProject(name)
        elif selected == 'p':
            showProjects()
        elif selected == 't':
            trackTime()
        elif selected == 's':
            stopTracking()
        elif selected == 'd':
            displayTotalTime()
        elif selected == 'e':
            sys.exit()
        else:
            print('\nERROR: No such option: {}\n'.format(selected))


def addProject(name):
    """
    Adding a project to the database.
    :param str name: Project name.
    """
    try:
        cursor.execute('insert into PROJECTS (name) values(?)', (name,))
    except sqlite3.IntegrityError:
        print('\nProject "{}" already is in the database.'.format(name))
    else:
        cursor.execute(
            'insert into TOTAL_TIME (project_name) values(?)', (name,))
        print('\n--- {} added ---\n'.format(name))
        connection.commit()


def showProjects():
    """
    Displaying projects in the database.
    """
    print('\nPROJECTS:\n---------')
    for project in getProjects():
        print('* ' + project[0])



def trackTime():
    """
    Track time beginning.
    """
    try:
        project_name = input('Enter project name: ')
    except KeyboardInterrupt:
        menu()
    if isProjectInDatabase(project_name):
        tracking = getTracking()
        if tracking:
            for record in reversed(tracking):
                if project_name in record:
                    if record[3] is None:
                        print('INFO: Project {} already tracked. Skipping.'
                            .format(project_name))
                        return
                    else:
                        _track(project_name)
                        return
                else:
                    _track(project_name)
                    return
        else:
            _track(project_name)
    else:
        print('ERROR: No such project: {}.'.format(project_name))
        while True:
            add = input('Do you want to add the project? [y/n] ').lower()
            if add == 'y':
                addProject(project_name)
                break
            elif add == 'n':
                break
            else:
                print('ERROR: No such option: {}.'.format(add))


def isProjectInDatabase(name):
    """
    Checking if project is already added to thr database.
    :param str name: Project name.
    :return: True or false.
    :rtype: bool
    """
    return name in [projects[0] for projects in getProjects()]


def getProjects():
    """
    Fetching projects from the database.
    :return: Projects.
    :rtype: list[tuple[str]]
    """
    cursor.execute('select name from PROJECTS')
    return cursor.fetchall()


def getTracking():
    """
    Getting tracking info.
    :return: Project name, start time, stop time.
    :rtype: list[tuple[str]]
    """
    cursor.execute('select * from TRACKING')
    return cursor.fetchall()


def _track(project_name):
    start_time = time.time()
    cursor.execute(
        'insert into TRACKING (project_name, start_time) values(?,?)',
        (project_name, start_time))
    connection.commit()
    print('\n--- tracking {} ---\n'.format(project_name))


def stopTracking():
    """
    Track time stop.
    """
    try:
        project_name = input('Enter project name: ')
    except KeyboardInterrupt:
        menu()
    if project_name in [c[0] for c in getProjects()]:
        tracking = getTracking()
        if tracking:
            for record in tracking:
                if project_name in record and record[3] is None:
                    _stop(project_name)
                    return
            print(
                'WARNING: Cannot stop tracking {} - not running.'
                    .format(project_name))
        else:
            print('WARNING: Cannot stop tracking - no projects added.')
    else:
        print('WARNING: Cannot stop tracking - no such project.')


def _stop(project_name):
    stop_time = time.time()
    cursor.execute(
        'update TRACKING set stop_time = ? where project_name = ?',
        (stop_time, project_name))
    connection.commit()
    print('\n--- stopped tracking {} ---\n'.format(project_name))
    totalTime(project_name)


def totalTime(project_name):
    """
    Calculating total tracking time and inserting it to the database,.
    :param str project_name: Name of a project to calculate time for.
    """
    cursor.execute(
        'select * from TRACKING where project_name = ? and stop_time is not null',
        (project_name,))
    tracking_records = cursor.fetchall()
    total_time = 0
    for record in tracking_records:
        total_time += record[3] - record[2]
    cursor.execute('update TOTAL_TIME set time = ? where project_name = ?',
                   (total_time, project_name))
    connection.commit()


def displayTotalTime():
    """
    Displaying total tracked time for selected project or all projects.
    """
    try:
        project_name = input(
            'type project name (press enter to see all projects): ')
    except KeyboardInterrupt:
        menu()
    if project_name:
        if isProjectInDatabase(project_name):
            cursor.execute(
                'select time from TOTAL_TIME where project_name = ?',
                (project_name))
            project_data = cursor.fetchall()
            if project_data[0][0]:
                print(datetime.timedelta(seconds=round(project_data[0][0])))
            else:
                print('no data available')
        else:
            print('ERROR: No such project: {}.'.format(project_name))
    else:
        cursor.execute('select project_name, time from TOTAL_TIME')
        projects_data = cursor.fetchall()
        for data in projects_data:
            if data[1]:
                print('{}: {}'.format(
                    data[0], datetime.timedelta(seconds=round(data[1]))))


connection = createDatabase()
cursor = connection.cursor()
createTable('''
        create table if not exists PROJECTS (
            id integer primary key autoincrement,
            name text unique not null
        );''')
createTable('''
        create table if not exists TRACKING (
            id integer primary key autoincrement,
            project_name text,
            start_time real,
            stop_time real,
            foreign key(project_name) references PROJECTS(name)
        );''')
createTable('''
        create table if not exists TOTAL_TIME (
            id integer primary key autoincrement,
            project_name text unique not null,
            time real,
            foreign key(project_name) references PROJECTS(name)
        )''')
menu()
