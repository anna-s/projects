# -*- coding: utf-8 -*-
# created by Anna Sekuła on 23.07.18

"""
Walking through a folder tree and searching for files with a certain
extension. Copying these files from their location to a new folder.
"""


import os, sys, shutil


input_directory = input('enter input directory: ')
output_directory = input('enter output directory: ')
extension = input('enter extension: ')
if not os.path.exists(input_directory):
    print('ERROR: No such directory: {}. Quitting.'
              .format(input_directory))
    sys.exit()
if not os.path.exists(output_directory):
    print('ERROR: No such directory: {}. Quitting.'
              .format(output_directory))
    sys.exit()
if not extension.startswith('.'):
    extension = '.' + extension

for folder, subfolders, file_names in os.walk(input_directory):
    for file_name in file_names:
        print(file_name)
        if file_name.endswith('.jpg'):
            print('copying {} '.format(file_name))
            try:
                shutil.copy(os.path.join(folder, file_name), os.path.join(output_directory, file_name))
            except IOError:
                os.mkdir(output_directory)
                shutil.copy(os.path.join(folder, file_name), os.path.join(output_directory, file_name))
