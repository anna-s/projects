import pyinputplus, sys

ingredients = []
ingredients.append(
    pyinputplus.inputMenu(['sourdough', 'wheat', 'white'], 
                          'select bread type:\n', 
                          numbered=True))
ingredients.append(
    pyinputplus.inputMenu(['chicken', 'ham', 'tofu', 'turkey'],
                          'select protein type:\n', 
                          numbered=True))
if pyinputplus.inputYesNo(
        'do you want cheese?\n', 
        postValidateApplyFunc=lambda x: True if x == 'yes' else False):
    ingredients.append(
        pyinputplus.inputMenu(['cheddar', 'mozzarella', 'Swiss'], 
                              'select cheese type:\n', 
                              numbered=True))
ingredients.append(
    pyinputplus.inputYesNo('do you want lettuce?\n', 
                           postValidateApplyFunc=lambda x: 'lettuce' if x == 'yes' else None))
ingredients.append(
    pyinputplus.inputYesNo('do you want tomato?\n', 
                           postValidateApplyFunc=lambda x: 'tomato' if x == 'yes' else None))
ingredients.append(
    pyinputplus.inputYesNo('do you want mayonaisse?\n', 
                           postValidateApplyFunc=lambda x: 'mayonaisse' if x == 'yes' else None))
ingredients.append(
    pyinputplus.inputYesNo('do you want mustard?\n', 
                           postValidateApplyFunc=lambda x: 'mustard' if x == 'yes' else None))

ingredients = list(filter(None, ingredients))
if pyinputplus.inputYesNo(
        f'do you confirm your order of a sandwich with: {", ".join(ingredients)}?\n') == 'no':
    sys.exit()

count = pyinputplus.inputInt('how many sandwiches do you order?\n', 
                             min=1)

prices = {'sourdough': 1, 'wheat': 0.75, 'white': 0.5, 
          'chicken': 0.25, 'ham': 0.75, 'tofu': 1, 'turkey': 0.75, 
          'cheddar': 0.75, 'mozzarella': 0.5, 'Swiss': 1, 
          'lettuce': 0.5, 'tomato': 0.5, 
          'mayonaisse': 0.25, 'mustard': 0.25}

print(f'price: {count * sum([prices[key] for key in ingredients]):.2f}')
