"""
Finding cavities buried in proteins where an inhibitor could bind. The
approach is to fit spheres of a specified radius that do not overlap with
other atoms. Atoms are assumed to be points - radii of atoms are ignored.
The script generates list of spheres which can be visualised with an
external script viscavity.py.
Written in Python 3.7.
"""
## TODO: logger instead of printing

import argparse, subprocess, math


info = argparse.ArgumentParser(
    description='Finding pockets in proteins where an inhibitor could '
                'bind. The approach is to fit spheres of a specified '
                'radius that do not overlap with other atoms. Atoms '
                'are assumed to be points. The program generates list '
                'of spheres which can be visualised with an external '
                'script viscavity.py.')
info.add_argument('-pdb', required=True, help='input PDB file path')
arguments = vars(info.parse_args())


def generateTessellationSeeds():
    """
    Generating Voronoi tessellation seeds.
    The seeds are coordinates of heavy atoms excluding heteroatoms -
    PDB file lines starting with 'ATOM'.
    :returns: Voronoi tessellation seeds as PDB coordinates.
    :rtype: list[list[float]]
    """
    print('Generating tessellation seeds...')
    pdb_file = open(arguments['pdb'])
    seeds = []
    for line in pdb_file.readlines():
        if line.startswith('ATOM '):
            seeds.append([float(line[30:38]),
                          float(line[38:46]),
                          float(line[46:54])])
    pdb_file.close()
    return seeds


def generateQvoronoiInputFile(seeds):
    """
    A qvoronoi input file is a following file (order matters):
    - number of dimensions for tessellation,
    - number of tessellation seeds,
    - seeds coordinates.
    """
    print('Generating qvoronoi input file as seeds.xyz...')
    seeds_file = open('seeds.xyz', 'w')
    print(3, file=seeds_file)  ## number of dimensions
    print(len(seeds), file=seeds_file)
    seeds_file.write('\n'.join(' '.join(map(str, row)) for row in seeds))
    seeds_file.close()


def voronoiTessellation():
    """
    Using Voronoi tessellation to find centers of all possible spheres.
    qvoronoi parameters:
    p: print Voronoi vertices;
    FN: print list of vertices that belong to each input point.
    Output file:
    number of dimensions;
    number of vertices (N);
    subsequent N lines: vertices coordinates, one point per line;
    line N+1: number of of vertices lists, equal to the number of atoms M;
    subsequent M lines: each line starts with the number of Voronoi
    vertices, following numbers are vertex indices, negative values mean
    that the region extends into infinity and these negative values
    should be skipped.
    """
    subprocess.Popen('qvoronoi TI %s TO %s p FN' %
                     ('seeds.xyz', 'qvoronoi.xyz'), shell=True).wait()


def getVoronoiVertices():
    """
    Extracting Voronoi vertices from the file after tessellation.
    Performing reverse assignment vertex -> list of atoms.
    :returns: Voronoi vertices.
    :rtype: list[float, int]  ## TODO: find a better way
    """
    voronoi_vertices_file = open('qvoronoi.xyz')
    voronoi_vertices_file.readline()  ## omitting number of dimensions
    coordinates_count = int(voronoi_vertices_file.readline())
    vertices = []  ## list of vertices from qvoronoi output
    while coordinates_count > 0:
        vertices.append(
            list(map(float, voronoi_vertices_file.readline().split())))
        coordinates_count -= 1
    voronoi_vertices_file.readline()  ## omitting  number of vertices lists
    atom = 0
    ## assignment: vertex -> list of atoms:
    for indices in voronoi_vertices_file.readlines():
        ## 0 element is number of elements in line:
        for index in map(int, indices.split()[1:]):
            if index >= 0:  ## skipping negative values
                vertices[index].append(atom)
        atom += 1
    voronoi_vertices_file.close()
    return vertices


def spheresRadii(vertices, seeds):
    """
    Calculating radii of spheres created by tessellation.
    A radius is a distance to the nearest atom.
    :param list[float, int] vertices:
    :param list[list[float]] seeds:
    :returns:
    :rtype: list
    """
    ## TODO: is this correct?
    radii = []  ## all spheres - radii
    for vertex in vertices:
        vectors_sum = []
        for atom_index in vertex[3:]:
            ## seeds are pdb coordinates, atom is item form list of atoms:
            vectors_sum.append(math.sqrt(
                (vertex[0] - seeds[atom_index][0]) ** 2
                + (vertex[1] - seeds[atom_index][1]) ** 2
                + (vertex[2] - seeds[atom_index][2]) ** 2))
        radii.append(min(vectors_sum))  ## why?
    return radii


seeds = generateTessellationSeeds()
generateQvoronoiInputFile(seeds)
voronoiTessellation()
vertices = getVoronoiVertices()


def calculateSphereCentreVectors(vertices, seeds):
    """
    Calculating vectors spanning from the sphere centre to atoms used to
    create vertex.
    :param list[float, int]] vertices: Voronoi vertices.
    :param list[list[float]] seeds: Voronoi tessellation seeds.
    :returns: Vectors.
    :rtype: list[tuple(float))
    """
    sphere_centre_vectors = []
    for vertex in vertices:
        single_sphere_vectors = [(vertex[0] - seeds[atom][0],
                                  vertex[1] - seeds[atom][1],
                                  vertex[2] - seeds[atom][2])
                                 for atom in vertex[3:]]
        sphere_centre_vectors.append(single_sphere_vectors)
    return sphere_centre_vectors


# def pickSpheres():
"""
Eliminating spheres that are not buried and that are too small. Too small 
spheres are less than 3 Å. Spheres are not buried when sum of vectors 
spanning from a sphere centre to atoms used to create vertex is longer 
than any of these four vectors.
"""


sphere_centre_vectors = calculateSphereCentreVectors(vertices, seeds)
for single_sphere_vectors in sphere_centre_vectors:
    ## here sphere_centre_vectors list is added the sum of vectors
    sum_of_vectors = [0, 0, 0]  ## sum of vectors
    for vector in single_sphere_vectors:
        sum_of_vectors[0] += vector[0]
        sum_of_vectors[1] += vector[1]
        sum_of_vectors[2] += vector[2]
    single_sphere_vectors.append(sum_of_vectors)
for linia in sphere_centre_vectors:
    dllis = []    #list of vector length
    for vector in linia:
        ## vector length:
        dllis.append(math.sqrt(vector[0] ** 2 + vector[1] ** 2 + vector[2] ** 2))
    singlemin = min(dllis[0:4])  ## the shortest of vectors
    if dllis[4] < singlemin:  ## the last one is the sum of vectors
        linia.append('in')  ## buried
    else:
        linia.append('out')  ## not buried

prad = open('cavities.dat', 'w')
n = 0
count = 0  ## number of spheres
radii = spheresRadii(vertices, seeds)
for wtf_line in radii:
    if wtf_line >= 3 and sphere_centre_vectors[n][5] == 'in':
        count = count + 1
    n += 1
print(count, file=prad)
prad.close()
prad = open('cavities.dat', 'a')
n = 0
for wtf_line in radii:
    if wtf_line >= 3 and sphere_centre_vectors[n][5] == 'in':
        ## coordinates of the center of a sphere and its radius:
        print('%20.15f %20.15f %20.15f %15.12f'
              % (vertices[n][0], vertices[n][1], vertices[n][2], wtf_line), file=prad)
    n += 1
prad.close()
