import argparse, os, sys, csv


info = argparse.ArgumentParser(
    description='Removing duplicated rows from csv files.')
info.add_argument(
    '-p',
    required=True,
    help='full path to the input directory (may contain non-csv files)')
arguments = vars(info.parse_args())

try:
    os.mkdir('without_duplicates')
except FileExistsError:
    pass
os.chdir(os.path.join(os.getcwd(), 'without_duplicates'))

try:
    all_files_names = os.listdir(arguments['p'])
except FileNotFoundError:
    print('ERROR: No such directory: {}. Quiting.'.format(arguments['p']))
    sys.exit(1)

for file_name in all_files_names:
    if file_name.endswith('.csv'):

        input_file = open(os.path.join(arguments['p'], file_name))
        ## an order-preserving way of removing duplicates
        unique = []
        for element in [row for row in csv.reader(input_file)]:
            if element not in unique:
                unique.append(element)
        input_file.close()

        output_file = open(file_name, 'w')
        for row in unique:
            csv.writer(output_file).writerow(row)
        output_file.close()
