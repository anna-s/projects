import openpyxl.utils, openpyxl.styles, os


def getNumber():
    while True:
        try:
            number = int(input('enter a positive integer: '))
        except ValueError:
            print('not an integer')
        else:
            if number < 0:
                print('not a positive integer')
                continue
            return number


def writeHeaders(worksheet, number):
    worksheet.append(range(number + 1))
    for n in range(1, number + 1):
        worksheet.cell(n + 1, 1).value = n
    worksheet['A1'] = None  ## was 0


def writeQuotient(worksheet):
    for _row in worksheet.iter_rows(min_row=2, min_col=2):
        for _cell in _row:
            worksheet.cell(_cell.row, _cell.column).value = (
                worksheet.cell(1, _cell.column).value *
                worksheet.cell(_cell.row, 1).value)


def setCellStyle(number, worksheet):
    style = openpyxl.styles.Font(bold=True)
    for n in range(1, number + 1):
        worksheet.cell(1, n + 1).font = style
        worksheet.cell(n + 1, 1).font = style


def run():
    workbook = openpyxl.Workbook()
    worksheet = workbook.active
    print('creating multiplication table in a spreadsheet')
    number = getNumber()
    writeHeaders(worksheet, number)
    writeQuotient(worksheet)
    setCellStyle(number, worksheet)
    workbook.save('multiplication.xlsx')
    print('multiplication table saved to {}'
          .format(os.path.join(os.getcwd(), 'multiplication.xlsx')))


run()
