import random, numpy, matplotlib.pyplot, time


class Grid:
    
    def __init__(self, size, density):
        """
        Generating an empty matrix (0 is an empty cell).
        :param int size: x / y dimension (only square matrices allowed).
        :param int density: Percentage of blocked cells.
        """
        self.size = size
        self.density = density
        self.matrix = [[0 for n in range(self.size)] 
                       for m in range(self.size)]
        self.generate()
    
    def generate(self):
        """
        Randomly generating blocked cells (1 is a blocked cell) 
        with given density, 
        first checking if randomly selected field is already blocked.
        """
        blocked_cells_count = self.size * self.size * self.density / 100
        while blocked_cells_count > 0:
            x = random.randint(0, self.size - 1)
            y = random.randint(0, self.size - 1)
            if self.matrix[x][y] == 0:
                self.matrix[x][y] = 1
                blocked_cells_count -= 1

    def check(self):
        """
        Checking if percolation occured (at least one 2 in the last row).
        :returns: True if there is percolation, False if there is not.
        :rtype: bool
        """
        return (
            True if any(cell == 2 for cell in self.matrix[self.size - 1]) 
            else False)
   
    def fill(self, x, y):
        """
        Filling up neighbouring cells. 
        Making sure that percolation runs within matrix size. 
        Checking whether a cell may be filled up. 
        :param int x: Next x dimension index.
        :param int y: Next y dimension index. 
        :returns: True if there is a change, false if not.
        :rtype: bool
        """
        if y > 0:
            if self.matrix[y - 1][x] == 0:
                self.matrix[y - 1][x] = 2
                return True
        if y < self.size - 1:
            if self.matrix[y + 1][x] == 0:
                self.matrix[y + 1][x] = 2
                return True
        if x > 0:
            if self.matrix[y][x - 1] == 0:
                self.matrix[y][x - 1] = 2
                return True
        if x < self.size - 1:
            if self.matrix[y][x + 1] == 0:
                self.matrix[y][x + 1] = 2
                return True
        return False

    def percolate(self):
        """
        Performing actual percolation: 
        runing through the whole matrix 
        and filling it up with 2s if possible. 
        Starting with filling the first row. 
        "Change" checks if there is any new 2 - 
        if True, the method goes on, 
        if False, there is no free cell to place a 2.
        """
        for y in range(self.size):
            if self.matrix[0][y] == 0: 
                self.matrix[0][y] = 2
        change = True
        while change == True:
            change = False
            for x in range(self.size):
                for y in range(self.size):
                    if self.matrix[y][x] == 2:
                        if self.fill(x, y):
                            change = True

    def picture(self):
        """
        Showing an image of the grid.
        """
        start = time.time()
        picture = matplotlib.pyplot.imshow(
            numpy.array(self.matrix), 
            matplotlib.colors.LinearSegmentedColormap.from_list(
                'custom', ['white', 'black', 'red'], 3))
        picture.axes.get_xaxis().set_visible(False)
        picture.axes.get_yaxis().set_visible(False)
        matplotlib.pyplot.colorbar(ticks=[0, 1, 2])
        print('visualisation time:', time.time() - start)
        matplotlib.pyplot.show()


print('\nSimulating percolation for a grid with given size and density.')
print('-' * 62)
# size = int(input('Enter grid size: '))
# density = int(input('Enter blocked cells density (%): '))
grid = Grid(1000, 40)
p_start = time.time()
grid.percolate()
print('percolation time:', time.time() - p_start)
print('percolation:', grid.check())
grid.picture()
print()
