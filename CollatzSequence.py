# -*- coding: utf-8 -*-

# created by Anna Sekuła on 09.01.18

"""
Generating Collatz sequence.
"""


def collatz(number):
    transformed = number // 2 if number % 2 == 0 else 3 * number + 1
    print(transformed)
    return transformed


print('Generating Collatz sequence.')
n = int(input('Enter some number: '))
while n != 1:
    try:
        n = collatz(n)
    except ValueError:
        print('Not a number!')
