import itertools, openpyxl, rdkit.Chem.AllChem, \
    rdkit.ML.Cluster.Butina, rdkit.DataStructs, rdkit.Chem.Draw, \
    rdkit.Chem.Descriptors, matplotlib.pyplot, matplotlib.figure, \
    rdkit.SimDivFilters.rdSimDivPickers, threading


def substrates():
    """
    molport.txt contains selected molecules.
    :rtype: list[rdkit.Chem.rdchem.Mol]
    """
    molecules = open('molport.txt')
    __substrates = []
    for line in molecules.readlines():
        molecule = rdkit.Chem.MolFromSmiles(line.split()[1])
        if molecule:
            __substrates.append(molecule)
            if len(__substrates) == 10000:
                break
    molecules.close()
    return __substrates


def diverseSubstrates(molecules):
    _fingerprints = [rdkit.Chem.AllChem.GetMorganFingerprint(molecule, 2)
                     for molecule in molecules]

    def distanceFunction(n, m, _fingerprints=_fingerprints):
        return 1 - rdkit.DataStructs.DiceSimilarity(_fingerprints[n],
                                                    _fingerprints[m])

    return [molecules[index] for index in
            rdkit.SimDivFilters.rdSimDivPickers.MaxMinPicker().LazyPick(
                distanceFunction, len(_fingerprints), 200)]


def reactions():
    return [str(cell.value)
            for row in openpyxl.load_workbook('reactions.xlsx').active
            for cell in row if cell.column == 2 and cell.row > 1]


def generateProducts(_substrates):
    """
    :returns: list of products as rdkit.Chem.rdchem.Mol objects
    :rtype: list[rdkit.Chem.rdchem.Mol]
    """
    products = []
    products_file = open('products.txt', 'w+')
    used_reactions = []
    # print(reactions())
    reactions_as_objects = [
        rdkit.Chem.AllChem.ReactionFromSmarts(reaction_string)
        for reaction_string in reactions()]
    substrates_combined = itertools.product(_substrates, repeat=2)
    used_reactions_file = open('used_reactions.txt', 'w+')
    # while len(products) <= 10000:
    for reaction_object in reactions_as_objects:
        print(rdkit.Chem.AllChem.ReactionToSmarts(reaction_object))
        for compounds in substrates_combined:
            try:
                products_tuples = reaction_object.RunReactants(compounds)
            except ValueError:
                continue
            for products_tuple in products_tuples:
                for product_object in products_tuple:
                    product = rdkit.Chem.MolToSmiles(product_object)
                    if product in products:
                        print(product + " in products")
                        continue
                    products.append(product_object)
                    products_file.write(product + '\n')
                    reaction = rdkit.Chem.AllChem.ReactionToSmarts(reaction_object)
                    if reaction not in used_reactions:
                        used_reactions.append(reaction)
                    else:
                        print(reaction + " in reactions")
                    # if len(products) == 10000:
    products_file.close()
    for used_reaction in used_reactions:
        used_reactions_file.write(used_reaction + '\n')
    used_reactions_file.close()
    return products


def fingerprints(molecules):
    return [rdkit.Chem.AllChem.GetMorganFingerprintAsBitVect(
                molecule, 2, 1024)
            for molecule in molecules]


def distances(fingerprints):
    """
    Distances matrix.
    """
    return [distance for n in range(1, len(fingerprints))
            for distance in
            [1 - similarity
             for similarity in rdkit.DataStructs.BulkTanimotoSimilarity(
                fingerprints[n], fingerprints[:n])]]


def cluster(distances, fingerprints_count, cutoff):
    """
    Butina clustering is most efficient for large sets of molecules.
    :returns: Tuple of clusters, where each cluster is a tuple of ids.
    :rtype: tuple
    """
    return rdkit.ML.Cluster.Butina.ClusterData(distances,
                                               fingerprints_count,
                                               cutoff,
                                               True)


def displayStructure(molecule):
    rdkit.Chem.Draw.ShowMol(molecule)


def molarMasses(molecules):
    """
    :param list of rdkit.Chem.rdchem.Mol molecules:
    """
    return {rdkit.Chem.MolToSmiles(molecule):
            rdkit.Chem.Descriptors.ExactMolWt(molecule)
            for molecule in molecules}


def logP(molecules):
    """
    :param list of rdkit.Chem.rdchem.Mol molecules:
    """
    return {rdkit.Chem.MolToSmiles(molecule):
            rdkit.Chem.Descriptors.MolLogP(molecule)
            for molecule in molecules}


def molarMassVsLogP(mol_weight, log_p):
    """
    :param dict mol_weight:
    :param dict log_p:
    """
    return {weight: log_p[compound]
            for compound, weight in mol_weight.items()}


def draw(_values, plot_title, y_boundaries, y_label, x_label, file_name):
    """
    :param dict _values:
    """
    matplotlib.pyplot.figure(figsize=(10, 5))
    matplotlib.pyplot.scatter(_values.keys(), _values.values())
    matplotlib.pyplot.title(plot_title)
    if not x_label:
        matplotlib.pyplot.xticks([])
    if y_boundaries:
        matplotlib.pyplot.gca().set_ylim(y_boundaries)
    matplotlib.pyplot.ylabel(y_label)
    matplotlib.pyplot.xlabel(x_label)
    # matplotlib.pyplot.show()
    matplotlib.pyplot.savefig(file_name)


def saveToFile(file_name, molecules):
    writer = rdkit.Chem.SmilesWriter(file_name)
    for molecule in molecules:
        writer.write(molecule)


def diversity(molecules):
    """
    :param list molecules:
    """
    return [rdkit.DataStructs.FingerprintSimilarity(pair[0], pair[1])
            for pair in itertools.combinations(fingerprints(molecules), 2)]


_substrates = substrates()
diverse_substrates = diverseSubstrates(_substrates)
_products = generateProducts(_substrates)
substrates_molar_masses = molarMasses(_substrates)
draw(substrates_molar_masses, 'molar masses of substrates', [],
     'mass [g/mol]', '', 'substrates_molar_masses.png')
substrates_log_p = logP(_substrates)
draw(substrates_log_p, 'logP of substrates', [],
     'logP', '', 'substrates_logP.png')
draw(molarMassVsLogP(substrates_molar_masses, substrates_log_p),
     'molar masses vs logP of substrates', [], 'logP',
     'molar mass [g/mol]', 'substrates_molar_masses_logP.png')
products_molar_masses = molarMasses(_products)
draw(products_molar_masses, 'molar masses of products', [],
     'mass [g/mol]', '', 'products_molar_masses.png')
products_log_p = logP(_products)
draw(products_log_p, 'logP of products', [],
     'logP', '', 'products_logP.png')
draw(molarMassVsLogP(products_molar_masses, products_log_p),
     'molar masses vs logP of products', [], 'logP',
     'molar mass [g/mol]', 'products_molar_masses_logP.png')
saveToFile('diverse_substrates.smiles', diverse_substrates)
substrates_diversity = diversity(diverse_substrates)
matplotlib.pyplot.plot(substrates_diversity, 'c.')
matplotlib.pyplot.title('substrates diversity')
matplotlib.pyplot.xticks([])
matplotlib.pyplot.ylabel('fingerprint similarity')
matplotlib.pyplot.show()
matplotlib.pyplot.savefig('substrates_diversity.png')
products_diversity = diversity(_products)
matplotlib.pyplot.plot(products_diversity, 'c.')
matplotlib.pyplot.title('products diversity')
matplotlib.pyplot.xticks([])
matplotlib.pyplot.ylabel('fingerprint similarity')
matplotlib.pyplot.show()
matplotlib.pyplot.savefig('products_diversity.png')

draw
