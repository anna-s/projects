# -*- coding: utf-8 -*-

# created by Anna Sekuła on 22.02.18


def toString(i):
    """
    Converting list ot tuple to string.
    :param list | tuple i:
    :rtype: str
    """
    return ', '.join(i[:-1]) + ' and ' + i[-1]


print(toString(['apples', 'bananas', 'tofu', 'cats']))
