# -*- coding: utf-8 -*-
# created by Anna Sekuła on 24.07.18

"""
Monitoring large files.
"""

import os, sys


directory = input('enter full directory path: ')
if not os.path.exists(directory):
    print('ERROR: No such directory: {}. Quitting.'.format(directory))
    sys.exit()
size = input('enter file size in MB: ')
try:
    size = int(size)
except ValueError:
    print('ERROR: Invalid size: {}. Quitting.'.format(size))
    sys.exit()

for folder, subfolders, file_names in os.walk(directory):
    for file_name in file_names:
        full_path = os.path.join(folder, file_name)
        if os.path.islink(full_path):
            pass
        else:
            _size = round(os.path.getsize(full_path) * 10**(-6), 2)
            if _size >= size:
                print('LARGE FILE FOUND\t{}\tOF SIZE {} MB'
                          .format(full_path, _size))
